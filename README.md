# WiIOSLibrary

[![CI Status](http://img.shields.io/travis/Stefan Kruger/WiIOSLibrary.svg?style=flat)](https://travis-ci.org/Stefan Kruger/WiIOSLibrary)
[![Version](https://img.shields.io/cocoapods/v/WiIOSLibrary.svg?style=flat)](http://cocoapods.org/pods/WiIOSLibrary)
[![License](https://img.shields.io/cocoapods/l/WiIOSLibrary.svg?style=flat)](http://cocoapods.org/pods/WiIOSLibrary)
[![Platform](https://img.shields.io/cocoapods/p/WiIOSLibrary.svg?style=flat)](http://cocoapods.org/pods/WiIOSLibrary)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WiIOSLibrary is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "WiIOSLibrary"
```

## Author

Stefan Kruger, stefan@kruline.com

## License

WiIOSLibrary is available under the MIT license. See the LICENSE file for more info.
